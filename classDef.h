#include <iostream>
#include <stdexcept>

class Dustin
{
public:
	static int objectCount;
	Dustin() {
		mHeight = 0;
		mWeight = 0;
		objectCount++;
	}
	Dustin(int inWeight = 1, int inHeight = 1) 
	{
		if(inWeight > 0) {
			mWeight = inWeight;
		}
		else
		{
			throw std::invalid_argument("Weight is out of range");
		}
		if(inHeight > 0) {
			mHeight = inHeight;
		}
		else
		{
			throw std::invalid_argument("Height is out of range");
		}
		objectCount++;
	}
	void setHeight(int);
	void setWeight(int);

	~Dustin() {
		std::cout << "Object destroyed" << std::endl;
		objectCount--;
	}
private:
	int mHeight;
	int mWeight;
}