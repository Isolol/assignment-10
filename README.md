You can modify the Dog class or create a new class. I would advise you to create another class. This class should have at least 2 attributes with the associated getters and setters.

In either case,  your class should have at least 2 constructors and a destructor. 1 constructor should take an argument for each attribute of your class. Each argument should have a default value.  In your constructor, error check the arguments and if there is a problem the throw an "invalid_argument"exception.  1 constructor should be a "copy constructor".

Add a static member to your class to keep track of how many instances of your class has been created.  If the count is even,  throw an string  (with a message) object as an exception .

In your destructor, print a message to standard out.


In you main, create some (at least 2) of the objects of your class. Each object should be created in is own try/catch block.  Make sure you catch the exceptions. When you catch the "invalid_argument" exception, print something to standard out. Whenyou catch the string exception, print something to cerr.